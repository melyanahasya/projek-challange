package com.example.demo.model;

import javax.persistence.*;

@Entity
@Table(name = "challange")
public class DB_Challange {
    //  membuat kolom Id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    //  membuat kolom Username
    @Column(name = "username")
    private String username;

    //  membuat kolom email
    @Column(name = "email")
    private String email;

    //  membuat kolom Password
    @Column(name = "password")
    private String password;

    //  membuat constructor kosong
    public DB_Challange() {
    }

    // membuat constructor
    public DB_Challange(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    // membuat getter end setter
    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

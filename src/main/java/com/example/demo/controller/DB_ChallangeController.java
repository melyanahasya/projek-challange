package com.example.demo.controller;

import com.example.demo.model.DB_Challange;
import com.example.demo.service.DB_ChallageService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

// membuat anotasi
@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class DB_ChallangeController {

    @Autowired
    private DB_ChallageService db_challageService;

    @Autowired
     private ModelMapper modelMapper;

    // membuat method untuk post login
    @PostMapping("/sign-in")
    public Map<String, Object> login(@RequestBody DB_Challange db_challange){
        return db_challageService.login(db_challange);
    }

    // membuat method untuk post register
    @PostMapping("/sign-up")
    public DB_Challange add(@RequestBody DB_Challange db_challange){
        return db_challageService.add(db_challange);
    }

    // membuat method delete
    @DeleteMapping("/{Id}")
    public void delete(@PathVariable("Id")Integer Id){
        db_challageService.deleteById(Id);
    }

    // membuat method getAll
    @GetMapping("/all")
    public List<DB_Challange> getAll() {
        return  db_challageService.getAll();
    }

    // membuat method get per Id
    @GetMapping("/{Id}")
    public DB_Challange get(@PathVariable("Id") Integer Id){
        return db_challageService.get(Id);
    }

    // membuat method edit
    @PutMapping("/{Id}")
    public DB_Challange edit(@PathVariable("Id") Integer Id,@RequestBody DB_Challange db_challange){
        return db_challageService.edit(Id, modelMapper.map(db_challange, DB_Challange.class));
    }
}

package com.example.demo.serviceImpl;

import com.example.demo.model.DB_Challange;
import com.example.demo.repository.DB_ChallangeRepository;
import com.example.demo.service.DB_ChallageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DB_ChallangeServiceImpl implements DB_ChallageService {

   // membuat anotasi autowired untuk menguhubungkan ke repository
    @Autowired
    DB_ChallangeRepository db_challangeRepository;


    @Override
    public Map<String, Object> login(DB_Challange db_challange) {
        DB_Challange db_challange1 = new DB_Challange(db_challange.getUsername(), db_challange.getEmail(), db_challange.getPassword());
        Map<String, Object> response =  new HashMap<>();
        return response;
    }


    // membuat method getAll
    @Override
    public List<DB_Challange> getAll() {
        return db_challangeRepository.findAll();
    }

    // membuat method add
    @Override
    public DB_Challange add(DB_Challange db_challange) {
        return db_challangeRepository.save(db_challange);
    }


    // membuat method get per Id
    @Override
    public DB_Challange get(Integer Id) {
        return db_challangeRepository.findById(Id).orElseThrow();
    }

    // membuat method delete
    @Override
    public void deleteById(Integer Id) {
     db_challangeRepository.deleteById(Id);
    }

    // membuat method edit
    @Transactional
    @Override
    public DB_Challange edit(Integer Id, DB_Challange db_challange) {
        db_challange.setPassword(db_challange.getPassword());
        DB_Challange update = db_challangeRepository.findById(Id).orElseThrow();
        update.setUsername(db_challange.getUsername());
        update.setEmail(db_challange.getEmail());
        update.setPassword(db_challange.getPassword());
        return update;
    }
}

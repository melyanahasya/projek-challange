package com.example.demo.service;

import com.example.demo.model.DB_Challange;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


public interface DB_ChallageService {

    Map<String, Object> login(DB_Challange db_challange);

    // membuat method getAll
    List<DB_Challange> getAll();

    // membuat method add
    DB_Challange add(DB_Challange db_challange);

    // membuat method get per Id
    DB_Challange get(Integer Id);

    // membuat method delete
    void deleteById(Integer Id);

    // membuat method edit
    @Transactional
    DB_Challange edit(Integer Id, DB_Challange db_challange);
}
